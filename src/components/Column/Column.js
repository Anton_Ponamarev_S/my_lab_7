import { useState } from 'react';
import { Card } from '..';

export function Column({ id, name, cards, onDelete, onAddCard, onDeleteCard }) {
  const [newCardName, setNewCardName] = useState('');

  const [isDeleteColumnLoading, setIsDeleteColumnLoading] = useState(false);
  const [isAddCardLoading, setIsAddCardLoading] = useState(false);

  /**
   * Метод, который вызовет удаление текущей колонки
   * (!) Храним состояние вызова в функции в isDeleteColumnLoading
   *  чтобы блокировать кнопку, когда удаление начинается
   */
  const handleDelete = async () => {
    setIsDeleteColumnLoading(true);

    /**
     * Вызовем здесь удаление колонки. Функция удаления пришла через пропс onDelete
     */

    setIsDeleteColumnLoading(false);
  };

  /**
   * Метод для добавления карточки.
   * Должен вызвать функцию onAddCard, которая пришла через пропсы.
   */
  const handleAddCard = async () => {
    setIsAddCardLoading(true);

    /**
     * Вызываем onAddCard
     * (!) Не забываем передать ей все необходимые аргументы!
     */

    setIsAddCardLoading(false);
  };

  const renderCards = () => {
    //Если у колонки нету карточек, то ничего не отображаем
    if (!cards) {
      return null;
    }

    //Иначе -- отобразим все карточки
    return cards.map((card) => (
      <Card
        //Передаем все нужные пропсы (см. компонент Card)
        key={card.id}
      />
    ));
  };

  return (
    <div style={{ border: '1px solid tomato', padding: '16px' }}>
      <h1 style={{ marginBottom: '8px', borderBottom: '1px solid black' }}>
        {name}
      </h1>
      {/** Кнопка для удаления текущей колонки -- по клику вызываем метод handleDelete */}
      <button onClick={handleDelete} disabled={isDeleteColumnLoading}>
        Delete column
      </button>
      <div>{renderCards()}</div>
      <div>
        {/**Делаем формочку для добавления карточки в колонку
            Точно так же как и в App для добавления колонки
         */}
        <form>
          <input
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_card_input"
            placeholder="New card name"
            //Храним value в локальном состоянии
            value={}
            //Меняем на onChange-е
            onChange={}
          />
          <button
            //! Не трогайте строку ниже -- она нужна для тестов !
            id="create_card_button"
            //По клику вызываем handleAddCard
            onClick={}
            type="button"
            //Отобразим состояние добавления карточки
            disabled={isAddCardLoading}
          >
            Add card
          </button>
        </form>
      </div>
    </div>
  );
}
