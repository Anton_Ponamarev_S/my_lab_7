export const promisify = async (data) => {
  return new Promise((resolve) => setTimeout(() => resolve(data), 1000));
};
