import React from 'react';
import { render } from '@testing-library/react';
import { fireEvent, waitFor } from '@testing-library/react';
import { it, expect } from '@jest/globals';
import App from '../App';
import { act } from 'react-dom/test-utils';

const DELETED_CARD_NAME = 'Card to delete';
const DELETE_CARD_BUTTON_CONTENT = 'Delete card';

jest.mock('../api', () => {
  return {
    api: {
      getCurrentUser: async () => {
        return { name: 'John Doe' };
      },
      getColumns: async () => {
        return [
          {
            name: 'Some column',
            cards: [{ id: Date.now(), name: DELETED_CARD_NAME }],
          },
        ];
      },
      deleteCard: async () => {
        return true;
      },
    },
  };
});

it(`Card должен содержать кнопку для удаления карточки
    По клику на кнопку -- карточка удаляется`, async () => {
  const { findByText, queryByText } = render(<App />);

  const deleteCardButton = await findByText(DELETE_CARD_BUTTON_CONTENT);

  await act(async () => {
    fireEvent.click(deleteCardButton);
  });

  await waitFor(() => expect(queryByText(DELETED_CARD_NAME)).toBeFalsy());
});
